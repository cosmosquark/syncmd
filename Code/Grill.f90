! Program Griller 
! Copyright (c) 2012 S.Pasetto, C.Chiosi & D.Kawata
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! If you use this program, please cite the associated paper
!
! Pasetto S. Chiosi C. and Kawata D., 2012, A&A, 545A, 14P
!
! comments along the text refere to equation in this paper.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! This program is free software; you can redistribute it and/or modify it under the terms of the 
! GNU General Public License as published by the 
! Free Software Foundation; 
! either version 2 of the License, or (at your option) any later version.
!
! This program is distributed WITHOUT ANY WARRANTY; 
! without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
! See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this program; 
! if not, write to the: 
! Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Comments and bugs report
! 
! Program to generate binned CMD, frequences for interval of mag and colour
! The program read a database of SSP previously generated. It reads as input the following files:
! Please, seach for "<<<" in order to midify mag and colour bands.
!
!
! Release comments/changes:
!
! Ver 0.0.1, 06 Sep 2012: introduced character variable dummyText to read input files
!                         to extend compatibility to gfortran compilers. 
! Ver 0.0.2, 18 Sep 2012: improved the counter for the light contribution, file 
!                         Output_NamePart_ unit 45, improved Norma text output 
! Ver 0.0.3, 05 Nov 2012: major improved code decription, comments etc...
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Input files:  
! Input_ListFilesNbodyStarParticles_Num_Loc.dat :: 
! Name of the files containing the stars of the input N-body simulations. It is not provided with 
! the released package but the test simulation studied in the quoted paper Pasetto et al 2012 can 
! be provided upon request.
! 
! Input_SSPNameLocation::
! It provides the location of each SSP CMD with other relative properties. It is a 3 column file, 
! with age metallicity and the corresponding subfolder location for the relative SSP CMD. Each 
! one of these files contain the stars properties (mass, log, bands U,B,�). Depending on the 
! desired band (U,B,V�) you are supposed to change the column number that you are going to read 
! with this program in the files of provided database (e.g. col 13 for band B, col 14 for band V  
! etc�)
!
! Output files:
!
! Ouput_NomefilesCMDgrilled.dat :: 
! output names of the Griller. For several input names in Input_ListFilesNbodyStarParticles_Num_Loc.dat 
! you can put here the corresponding output file names (e.g., if you want to implement the evoling 
! sequence of CMDs with time)
!
! Output_NamePart_OutputCMDdone.dat :: 
! file with the list of N-body particles that enter with at least a bit of light in the CMD observed
!
! Output_MasseSSP_Tot_Metal_Age.dat :: 
! file containing the metallicity index, the age index and the total mass of the SSP
!
! Output_MassSSP_Ini_vs_Current_Metal_Age.dat ::
! file containing for each age and metallicity the corresponding indexes the initila mass and the 
! current (evolved) mass.
!
! Output_NamePart_Output_CMDdone ::
! File containing the particle number that actually passed the magnitude selection criteria, to track 
! back with particle of your simulation contribute to the final CMD.


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Main program script
PROGRAM Griller
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Modules:  
! Two modules from the Numerical Recipes (Press et al 1992,1996) has to be read, e.g., from 
! http://www.nr.com/ 
! Copyright and other Restrictions can apply depending on the nature of your project.
  USE nrtype
  USE nrutil, ONLY : assert_eq,iminloc,nrerror  

! Modules with global variables
  USE Globals

! Local variables
  IMPLICIT NONE
  INTEGER :: ColCase1, ColCase2, MagCase
  REAL(DP) :: MagLimitCMD1, MagLimitCMD2, ColLimitCMD1, ColLimitCMD2, LogGLimit1, LogGLimit2
  CHARACTER(len=100),ALLOCATABLE,DIMENSION(:,:) :: SSPNameLoc
  CHARACTER(len=100), ALLOCATABLE, DIMENSION(:) :: Current_Nbody_InputFileName, Current_CMD_OutputFileName
  CHARACTER(len=100) :: header
  CHARACTER(len=9) :: dummyText
  REAL(DP),DIMENSION(NTotSTAR) ::  XCol1, XCol2, XMag_S, AgeStar,logL_Lo,LogTe,LogG,MassIni,CurrentMass,ZetaMetalli,YHe,C_O, &
                                                VMU,VMB,VMV,VMR,VMI,VMJ,VMH,VMK,KCO,IIDU, ug, gr, ri, rz, g, r
  REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: MassSSP_Ini, MassSSP_Current,MassSSP_ORI,MassSSP_Tot,Pos_NbodyPart,Grid_Sum_CMD, &
										   Grid_Sum_CMDApp     
  REAL(DP),ALLOCATABLE,DIMENSION(:) :: XCol,XMag,XColApp,XMagApp,MagDiff,ColDiff,Mass_NbodyPart,Age_NbodyPart,Metal_NbodyPart, &
									   Sl,Sb,SDist,SNh,SAv
  REAL(DP) :: XColor, XMagnitude, sumgrid, &
              MassSSP_Current_Age, MassSSP_Ini_Age, sommaTot, & 
              x,y,z, vx, vy, vz, eps, &
              ch11, ch12, ch13, ch14, ch15, ch16, ch17, ch18, ch19, &
              name, star_part_mass,  LoSSPAge, UpSSPAge, LoSSPZeta, UpSSPZeta, &
              TrueAge, TrueZeta, GridValue_Zeta1_Age1, GridValue_Zeta1_Age2, GridValue_Zeta2_Age1, GridValue_Zeta2_Age2, gvz1, & 
			  gvz2, gvint1, gvint2, &
              gvintfin, grid_comp_inter,MagAppMin,ColAppMin,MagAppMax,ColAppMax
  INTEGER, ALLOCATABLE, DIMENSION(:,:,:,:) :: Grid
  INTEGER :: i,j,NStepCol,NStepMag,NStepColApp,NStepMagApp,IndMet,IndAge,ICol,IMag,IGrid,JGrid,IndCMDdone,nstar,Ind_NbodyPart, &
			NPart,MaxNumCMDtodo,IndZetaBraket,IndAgeBraket,IndMinMagDiff,IndMinColDiff
  INTEGER, ALLOCATABLE, DIMENSION(:) :: IndAgeSSP_NbodyPart, IndMetalSSP_NbodyPart, NamePart
  LOGICAL :: IamIn

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! First open IDL defined global variables, including col and mag. selection
    OPEN(70,file='IDL_GLOBALS.dat', status='OLD')
        READ(70,*) ColCase1, ColCase2, MagCase, MagLimitCMD1, MagLimitCMD2, &
		   ColLimitCMD1, ColLimitCMD2, LogGLimit1, LogGLimit2
   CLOSE(70)
!
  !Open files with names and locations of all the SSP in the HRD
  OPEN(10, file='Input_SSPNameLocation.dat', status ='OLD') 

  !Read number of ages and metallicity in the database
  READ(10,'(I5,I5)') nAges,nZeta
  ALLOCATE(SSPNameLoc(nAges,nZeta), SSPAge(nAges), SSPZeta(nZeta), MassSSP_Tot(nAges,nZeta), &
           MassSSP_ORI(nAges,nZeta), MassSSP_Ini(nAges,nZeta), MassSSP_Current(nAges,nZeta))
  SSPAge=0.
  SSPZeta=0.
  MassSSP_Tot=0.
  MassSSP_ORI=0.
  MassSSP_Ini=0.
  MassSSP_Current=0.  
  !reading
  DO j = 1, nZeta
     DO i = 1, nAges
        READ(10,'(F15.1,5X,F6.4,5X,A100)') SSPAge(i),SSPZeta(j),SSPNameLoc(i,j)
     ENDDO
  ENDDO

  ![year] for the ages of SSP
  SSPAge(:)=SSPAge(:)*1.e6

  !base vector absolute colour
  NStepCol= INT((ColSup - ColInf)/StepCol) +1
  ALLOCATE(XCol(NStepCol))
  XCol(1)= ColInf
  DO j=2,NStepCol
     XCol(j)= XCol(j-1) + StepCol
  ENDDO

  !base vector apparent colour
  NStepColApp = INT((ColSupApp - ColInfApp)/StepColApp) +1
  ALLOCATE(XColApp(NStepColApp),ColDiff(NStepColApp))
  ColDiff=0.0
  XColApp(1)= ColInfApp
  DO j=2,NStepColApp
     XColApp(j)= XColApp(j-1) + StepColApp
  ENDDO

  !base vector absolute mag
  NStepMag= INT((MagSup - MagInf)/StepMag) +1
  ALLOCATE(XMag(NStepMag))
  XMag(1)=MagInf
  DO j=2,NStepMag
     XMag(j)= XMag(j-1)+StepMag
  ENDDO
  
  !base vector apparent mag 
  NStepMagApp= INT((MagSupApp - MagInfApp)/StepMagApp) +1
  ALLOCATE(XMagApp(NStepMagApp),MagDiff(NStepMagApp))
  MagDiff=0.0
  XMagApp(1)=MagInfApp
  DO j=2,NStepMagApp
     XMagApp(j)= XMagApp(j-1)+StepMagApp
  ENDDO

  !Define an array for eq. 19. with the number of bins per colour and mag
  !Grid (total num ages, tot num metallicity, number colour bins, number mag bins)
  ALLOCATE(Grid(nAges,nZeta,NStepCol,NStepMag))
  Grid = 0
  
  !Compute the normalization constant of the SSP to calculate its total mass
  CALL NORMA 

  !Output files
  OPEN(65,file='Output_MassSSP_Ini_vs_Current_Metal_Age.dat',status='unknown')
  OPEN(44,file='Output_MasseSSP_Tot_Metal_Age.dat',status='unknown')
  WRITE(*,223)
  223 FORMAT(5x,'Starting to upload the Database')
  !metallicity cycle
  IF (.TRUE.) WRITE(*,*) "Attention: some cuts to fit within the database range have been applied"
  MetalLoop_Grid: DO IndMet = 1,nZeta
     WRITE(*,777)  FLOAT(IndMet)/REAL(nZeta)*100
     777  FORMAT(2x,'Percentage completed close to ',1F10.2,'%')
     AgeLoop_Grid: DO IndAge=1,nAges
        OPEN(20,FILE=SSPNameLoc(IndAge,IndMet), STATUS='OLD')
        !10 rows header read
        DO i=1,10
           READ(20,'(A150)') Header
        ENDDO
        !read all values
        loopStelle:DO i=1,NTotSTAR !read all the star from the SSP with current metallicity index IndMet and age index IndAge
			READ(20,*) AgeStar(i),logL_Lo(i),LogTe(i),LogG(i),MassIni(i),CurrentMass(i),ZetaMetalli(i),YHe(i),C_O(i),KCO(i), &
					   IIDU(i),VMU(i),VMB(i),VMV(i),VMR(i),VMI(i),VMJ(i),VMH(i),VMK(i)
!
SELECT CASE (ColCase1)
	CASE (1)
		XCol1(i) = VMU(i)
	CASE (2)
		XCol1(i) = VMB(i)
	CASE (3)
		XCol1(i) = VMV(i)
	CASE (4)
		XCol1(i) = VMR(i)
	CASE (5)
		XCol1(i) = VMI(i)
	CASE (6)
		XCol1(i) = VMJ(i)
	CASE (7)
		XCol1(i) = VMH(i)
	CASE (8)
		XCol1(i) = VMK(i)						
	CASE (9)
		XCol2(i) = 1.28*(VMU(i)-VMB(i))+1.13	    ! ug(i)
	CASE (10)
		XCol2(i) = 1.02*(VMB(i)-VMV(i))-0.22	    ! gr(i)		
	CASE (11)
		XCol2(i) = 0.91*(VMR(i)-VMI(i))-0.20	    ! ri(i)
	CASE (12)
		XCol2(i) = 1.72*(VMR(i)-VMI(i))-0.41	    ! rz(i)
END SELECT
!
SELECT CASE (ColCase2)
	CASE (1)
		XCol2(i) = VMU(i)
	CASE (2)
		XCol2(i) = VMB(i)
	CASE (3)
		XCol2(i) = VMV(i)
	CASE (4)
		XCol2(i) = VMR(i)
	CASE (5)
		XCol2(i) = VMI(i)
	CASE (6)
		XCol2(i) = VMJ(i)
	CASE (7)
		XCol2(i) = VMH(i)
	CASE (8)
		XCol2(i) = VMK(i)
	CASE (9)
		XCol2(i) = 0
	CASE (10)
		XCol2(i) = 0						
	CASE (11)
		XCol2(i) = 0
	CASE (12)
		XCol2(i) = 0
END SELECT
!
SELECT CASE (MagCase)
	CASE (1)
		XMag_S(i) = VMU(i)
	CASE (2)
		XMag_S(i) = VMB(i)
	CASE (3)
		XMag_S(i) = VMV(i)
	CASE (4)
		XMag_S(i) = VMR(i)
	CASE (5)
		XMag_S(i) = VMI(i)
	CASE (6)
		XMag_S(i) = VMJ(i)
	CASE (7)
		XMag_S(i) = VMH(i)
	CASE (8)
		XMag_S(i) = VMK(i)
	CASE (9)
		XMag_S(i) = VMV(i) + 0.60*(VMB(i)-VMV(i))-0.12	! g(i)
	CASE (10)
		XMag_S(i) = VMV(i)-0.42*(VMB(i)-VMV(i))+0.11	! r(i)								
END SELECT
!					
			IF (.FALSE.) THEN
				IF ( &
					((VMI(i)>-4.-0.12).AND.(VMI(i)<-4.+0.12)) .AND. &
					((VMV(i)-VMI(i)>1.).AND.(VMV(i)-VMI(i)<2.5)) .AND. &
					((LogG(i)>LogGLimit1).AND.(LogG(i)<LogGLimit2)) &
					) THEN
					XColor= XCol1(i) - XCol2(i) ! <<< choose colours
  					ICol=0
					!find corresponding colour bin where to locate the star
					CALL Bracket_COL(NStepCol,XCol, XColor, ICol)
					IGrid = ICol
					XMagnitude = XMag_S(i) !<<< mag
					IMag=0
					!find mag bin
					CALL Bracket_MAG(NStepMag,XMag, XMagnitude, IMag)
					JGrid = IMag
					!add the star 
					Grid(IndAge,IndMet,ICol,IMag) = Grid(IndAge,IndMet,ICol,IMag)+1
				ELSE
					CYCLE loopStelle
				ENDIF
			ELSE
				IF ( &
					((LogG(i)>LogGLimit1).AND.(LogG(i)<LogGLimit2)) &
					) THEN
				XColor= XCol1(i) - XCol2(i) !<<< choose colours
  				ICol=0
				!find the corresponding colour bin for the star
				CALL Bracket_COL(NStepCol,XCol, XColor, ICol)
				IGrid = ICol
				XMagnitude = XMag_S(i) !<<< chose the mag.
				IMag=0
				!find the mag bin 
				CALL Bracket_MAG(NStepMag,XMag, XMagnitude, IMag)
				JGrid = IMag
				!add the star... as before.
				Grid(IndAge,IndMet,ICol,IMag) = Grid(IndAge,IndMet,ICol,IMag)+1
				ENDIF
			ENDIF
        ENDDO loopStelle
        MassSSP_Ini_Age=SUM(MassIni)
        MassSSP_Current_Age=SUM(CurrentMass)        
		!end reading stars of the SSP

        READ(20,1940) dummyText,sommaTot
        1940 FORMAT(1x,A9,E8.2)
        !calculate mass initial and current of the SSP
        MassSSP_ORI(IndAge,IndMet)= sommaTot !original mass
        MassSSP_Ini(IndAge,IndMet)= MassSSP_Ini_Age
        MassSSP_Current(IndAge,IndMet)= MassSSP_Current_Age
        CLOSE (20)

        !write output
        MassSSP_Tot(IndAge,IndMet) = MassSSP_Current(IndAge,IndMet) + NStar_UnderMl * Mean_Mass_UnderMl
        WRITE(65,999) IndAge,IndMet,MassSSP_Ini(IndAge,IndMet),MassSSP_Current(IndAge,IndMet)
        WRITE(44,225) IndMet,IndAge,MassSSP_Tot(IndAge,IndMet)        
     ENDDO AgeLoop_Grid
  ENDDO MetalLoop_Grid
  225 FORMAT(5x, 'IndMet=  ', i10,5x, 'IndAge=  ', i10,5x,'MassSSP_Tot=  ', 1e12.3)
  999 FORMAT(5x, 2i5, 5x, 1p1e12.3, 5x,1p1e12.3)
  CLOSE(44)
  CLOSE(65)
  ! Read N-body simulations
  OPEN(23, file='Input_ListFilesNbodyStarParticles_Num_Loc.dat',status='OLD')
  OPEN(24, file='Ouput_NomefilesCMDgrilled.dat',status='unknown')
  READ(23,*) MaxNumCMDtodo
  READ(24,*) MaxNumCMDtodo
       WRITE(*,*)'Number of CMDs to process:', MaxNumCMDtodo
  ALLOCATE(Current_Nbody_InputFileName(MaxNumCMDtodo),Current_CMD_OutputFileName(MaxNumCMDtodo))
  DO i=1, MaxNumCMDtodo ! <<< depend on the number of names in Input_ListFilesNbodyStarParticles_Num_Loc.dat
     READ(23,'(a100)') Current_Nbody_InputFileName(i)
     READ(24,'(a100)') Current_CMD_OutputFileName(i)   
  ENDDO
  CLOSE(23)
  CLOSE(24)

  CMDtoDO:DO IndCMDdone=1,MaxNumCMDtodo
     WRITE(*,*)'Loop on the no CMD to do, current: ', IndCMDdone,' on ', MaxNumCMDtodo
     WRITE(*,'(a150)')Current_CMD_OutputFileName(IndCMDdone)
     !  read mass, formation time and metallicity of the star-particles
     !  NPart = max num star-particles
     OPEN(25,file=Current_Nbody_InputFileName(IndCMDdone), status='OLD')
     !   1   2   3   4   5   6   7   8     9   10   11-19   20
     !   x   y   z   mp  vx  vy  vz  tborn eps  z   chemo   name 
     NPart=0 ! n of particles
     DO
        READ(25,*,END=600) x,y,z,x,x,x,x,x,x,x,x,ch11,ch12,ch13,ch14,ch15,ch16,ch17,ch18,ch19,name,x,x,x,x,x
        NPart=NPart+1
     ENDDO
     600 CONTINUE
     ALLOCATE(Pos_NbodyPart(NPart,3),Mass_NbodyPart(NPart),Age_NbodyPart(NPart),Metal_NbodyPart(NPart), &
              IndAgeSSP_NbodyPart(NPart),IndMetalSSP_NbodyPart(NPart),Sl(NPart),Sb(NPart),SDist(NPart),SNh(NPart),SAv(NPart), &
			  NamePart(NPart))
     REWIND(25)
     i=0
     letturaNbody: DO i=1,NPart
        READ(25,*) &
            Pos_NbodyPart(i,1),Pos_NbodyPart(i,2),Pos_NbodyPart(i,3), & !in kpc
            Mass_NbodyPart(i), &            !in solar mass
            eps, &                          !necessary
            vx,vy,vz, &                     !not stored upto now
            Age_NbodyPart(i), &             !age
            eps, &                          !
            Metal_NbodyPart(i), &           !metallicityX
            ch11, ch12, ch13, ch14, ch15, ch16, ch17, ch18, ch19, & !
            name, &
            Sl(i),Sb(i),SDist(i),SNh(i),SAv(i)                            !
!        SAv(i)=0.              !Make column density equal to zero
        ! if the particle goes outside... no sense... put it back
        IF (Metal_NbodyPart(i) < SSPZeta(1))     Metal_NbodyPart(i)=SSPZeta(1)     !outside database
        IF (Metal_NbodyPart(i) > SSPZeta(NZeta)) Metal_NbodyPart(i)=SSPZeta(NZeta) !outside database
        !age in years, accounting for cosmological simulation as t(presentday)=0
        Age_NbodyPart(i)=(Age_NbodyPart(i))*1.0e9
        IF (Age_NbodyPart(i) < SSPAge(1))     Age_NbodyPart(i)=SSPAge(1)
        IF (Age_NbodyPart(i) > SSPAge(NAges)) Age_NbodyPart(i)=SSPAge(NAges)
        
        !for each star-particle its age and metallicity
        IndZetaBraket=0
        CALL Bracket_metal(Metal_NbodyPart(i),IndZetaBraket)
        IndMetalSSP_NbodyPart(i) = IndZetaBraket
        IndAgeBraket=0
        CALL Bracket_age(Age_NbodyPart(i), IndAgeBraket)
        IndAgeSSP_NbodyPart(i) = IndAgeBraket
        nstar=i
		NamePart(i) = i
     ENDDO letturaNbody
     SDist(:)=SDist(:)*1000 !distances in parsec
     !total mass SSP
     star_part_mass=SUM(Mass_NbodyPart(:))
     WRITE(*, 1111) star_part_mass
     1111 FORMAT(5x, 'Total stellar mass =  ', 1p1e12.3) 

     !Here we have a lot of work done to sum over the SSP of different ages and metallicity eq. (3):
	 !-weight over the masses by eq. (16)
	 !-interpolation 
	 !-Eq. (19)
     ALLOCATE(Grid_Sum_CMD(NStepCol,NStepMag),Grid_Sum_CMDApp(NStepColApp,NStepMagApp))
     Grid_Sum_CMD=0.0_dp
     Grid_Sum_CMDApp=0.0_dp
     WRITE(*,*) 'Looping on the Nbody Particles'
	 OPEN(45,file=trim(Current_CMD_OutputFileName(IndCMDdone))//'_OutputNamePart.dat',status='unknown')
     NbodySSPloop: DO Ind_NbodyPart=1,NPart
        ! screen output "<<<" adjust as you like
		IamIn = .FALSE. !logical variable to account for the particle to be "in" or "out" the CMD for at least a little bit of light
        IF (mod(Ind_NbodyPart,10000)== 0) WRITE(*,*) "Current Particle", Ind_NbodyPart
        IndMet   = IndMetalSSP_NbodyPart(Ind_NbodyPart) !metallicity index of the SSP corresponding to Ind_NbodyPart 
        IndAge   = IndAgeSSP_NbodyPart(Ind_NbodyPart) !age index of the SSP corresponding to Ind_NbodyPart 
        LoSSPAge = SSPAge(IndAge) !SSP with age soon below to the age of the NbodyPart
        UpSSPAge = SSPAge(IndAge+1) !SSP with age soon above to the age of the NbodyPart
        LoSSPZeta   = SSPZeta(IndMet) !same for metallicity 
        UpSSPZeta   = SSPZeta(IndMet+1) !
        TrueAge   = Age_NbodyPart(Ind_NbodyPart) !age NbodyPart
        TrueZeta = Metal_NbodyPart(Ind_NbodyPart) !metallicity NbodyPart
        
        MagAppMin = MagInf + 5.0 * LOG10(SDist(Ind_NbodyPart)) - 5.0 + SAv(Ind_NbodyPart) !apparent mag
        MagAppMax = MagSup + 5.0 * LOG10(SDist(Ind_NbodyPart)) - 5.0 + SAv(Ind_NbodyPart) 
        MagDiff = XMagApp - MagAppMin
        IndMinMagDiff = iminloc(ABS(MagDiff))
        
        ColAppMin = ColInf + (1.0 - 0.479) * SAv(Ind_NbodyPart)
        ColAppMax = ColSup + (1.0 - 0.479) * SAv(Ind_NbodyPart)
        ColDiff = XColApp - ColAppMin
        IndMinColDiff = iminloc(ABS(ColDiff))
		
		!slightly complex but it works: it at least a mag bin  of the particle under consideration pass the selection criteria
		!of MagLimitCMD+- then it is true for the most brilliant bin:
		MagLoop: DO j=1,NStepMag  
		   IF ( (XMag(j) + 5.0 * LOG10(SDist(Ind_NbodyPart)) - 5.0 + SAv(Ind_NbodyPart) <  MagLimitCMD1 &
		        .OR.  &
		         XMag(j) + 5.0 * LOG10(SDist(Ind_NbodyPart)) - 5.0 + SAv(Ind_NbodyPart) >= MagLimitCMD2)) CYCLE MagLoop
           ColourLoop: DO i=1,NStepCol
           	  IF ( (XCol(i) + (1.0 - 0.479) * SAv(Ind_NbodyPart) <  ColLimitCMD1 &
		           .OR.  &
		            XCol(i) + (1.0 - 0.479) * SAv(Ind_NbodyPart) >= ColLimitCMD2)) CYCLE ColourLoop
              !2-point interpolation style: interpolate over the Gird values loaded in the previous steps for the current particle Ind_NbodyPart
              GridValue_Zeta1_Age1 = Grid(IndAge  ,IndMet  ,i,j) !frequency CMD(i,j) per {age,metallicity} = {IndAge,IndMet} 
              GridValue_Zeta1_Age2 = Grid(IndAge+1,IndMet  ,i,j) ! the same 
              GridValue_Zeta2_Age1 = Grid(IndAge  ,IndMet+1,i,j) ! ...
              GridValue_Zeta2_Age2 = Grid(IndAge+1,IndMet+1,i,j) ! ...
              !if I have zero everywhere, it's useless to interpolate and I cycle to save time
              IF ( (GridValue_Zeta1_Age1 == 0) .AND. &
                   (GridValue_Zeta1_Age2 == 0) .AND. &
                   (GridValue_Zeta2_Age1 == 0) .AND. &
                   (GridValue_Zeta2_Age2 == 0)) CYCLE ColourLoop
              !interpolation between 2 ages ant the lower metallicity 
              CALL interpo(GridValue_Zeta1_Age1,GridValue_Zeta1_Age2,LoSSPAge,UpSSPAge,TrueAge,gvint1) 
              gvz1=gvint1
              !the same at the upper metallicity
              CALL interpo(GridValue_Zeta2_Age1,GridValue_Zeta2_Age2,LoSSPAge,UpSSPAge,TrueAge,gvint2) !
              gvz2=gvint2
              !final interopolation between the 2 previous values
              CALL interpo(gvz1,gvz2,LoSSPZeta,UpSSPZeta,TrueZeta,gvintfin)  
              grid_comp_inter=gvintfin
              !Eq. (19)
              Grid_Sum_CMD(i,j)= grid_comp_inter / MassSSP_Tot(IndAge,IndMet) * Mass_NbodyPart(Ind_NbodyPart) + Grid_Sum_CMD(i,j)
              Grid_Sum_CMDApp(i+IndMinColDiff,j+IndMinMagDiff)= grid_comp_inter / MassSSP_Tot(IndAge,IndMet) & 
																* Mass_NbodyPart(Ind_NbodyPart)  &
																+ Grid_Sum_CMDApp(i+IndMinColDiff,j+IndMinMagDiff)
			  IamIn=.TRUE.
           ENDDO ColourLoop
        ENDDO MagLoop
		IF (IamIn == .TRUE.) WRITE(45,'(I6)') Ind_NbodyPart !The particle contributed for at least bin in the light of the CMD
     ENDDO NbodySSPloop
	 CLOSE(45)
     
     OPEN(30,file=trim(Current_CMD_OutputFileName(IndCMDdone))//'_ABS.dat',status='unknown')
     ! store the final result
     DO i=1,NStepCol
        DO j=1,NStepMag
           WRITE(30,1000) XCol(i),XMag(j),Grid_Sum_CMD(i,j)
        ENDDO
     ENDDO
     CLOSE(30)
     OPEN(31,file=trim(Current_CMD_OutputFileName(IndCMDdone))//'_APP.dat',status='unknown')
     ! store
     DO i=1,NStepColApp
        DO j=1,NStepMagApp
           WRITE(31,1000) XColApp(i),XMagApp(j),Grid_Sum_CMDApp(i,j)
        ENDDO
     ENDDO
     1000 FORMAT(2x,2f10.3,5x,1p1e12.3)
     CLOSE(31)

     !final check
     sumgrid=SUM(Grid_Sum_CMD(:,:))
     WRITE(*, 444) sumgrid
     444  FORMAT(5x,'Total num stars in HRD with M>= 0.15 Msun =  ',1p1e12.4)
     DEALLOCATE(Pos_NbodyPart, Mass_NbodyPart, Age_NbodyPart, Metal_NbodyPart, &
                IndAgeSSP_NbodyPart, IndMetalSSP_NbodyPart,Sl,Sb,SDist,SNh,SAv,NamePart)
     DEALLOCATE(Grid_Sum_CMD, Grid_SUM_CMDApp)
  ENDDO CMDtoDO
  
END PROGRAM Griller
