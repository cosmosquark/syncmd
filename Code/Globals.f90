MODULE Globals  
    USE nrtype  
    IMPLICIT NONE
    INTEGER, PARAMETER :: NTOTSTAR = 100000 

!base vector in absolute colour	
	REAL(DP), PARAMETER :: ColInf = -1.
	REAL(DP), PARAMETER :: ColSup = 3
	REAL(DP), PARAMETER :: StepCol= 0.05

!base vector in apparent colour
	REAL(DP), PARAMETER :: ColInfApp = -1.
	REAL(DP), PARAMETER :: ColSupApp = 5.
	REAL(DP), PARAMETER :: StepColApp= 0.05

!base vector in absolute mag 
	REAL(DP), PARAMETER :: MagInf= -6.
	REAL(DP), PARAMETER :: MagSup = 9.
	REAL(DP), PARAMETER :: StepMag= 0.05

!base vector in apparent magnitude
	REAL(DP), PARAMETER :: MagInfApp= -1.
	REAL(DP), PARAMETER :: MagSupApp = 30.
	REAL(DP), PARAMETER :: StepMagApp= 0.05

    INTEGER :: NStar_UnderMl,NTOTSTAR_OverMl,NZeta,NAges        
    REAL(DP) :: Mean_Mass_OverMl, &
                Mean_Mass_UnderMl, &
                Mean_Mass_Stars, &
                TOTAL_MASS_SSP
    REAL(DP),ALLOCATABLE,DIMENSION(:) :: SSPAge,SSPZeta
 END MODULE Globals
