SUBROUTINE Bracket_age(agi,i)  
  !     N :: number of grid vector elements
  !     SSPAge :: gird age vector 
  !     Agi :: age variable in grid
  !     Ia :: index of the gird point lower than the Agi value
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Modules
  USE nrtype
  USE Globals

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Local variables
  IMPLICIT NONE
  INTEGER n,i,di,niter
  INTEGER iinf,isup,imed
  REAL(DP)  xinf,xsup,agi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Main text
  n=NAges
    ! begins

  IF (i.EQ.0) THEN
     niter=0
     iinf=1
     isup=n
     IF (agi.GT.SSPAge(iinf)) THEN
        IF (agi.GE.SSPAge(isup)) THEN
           iinf=isup-1
        ELSE
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (agi<=SSPAge(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  ELSE
     niter=0
     iinf=1
     isup=n
     IF (agi.GT.SSPAge(iinf)) THEN
        IF (agi.GE.SSPAge(isup)) THEN
           iinf=isup-1
        ELSE
           di=1
           IF (agi.GE.SSPAge(i)) THEN
              iinf=i
              isup=MIN(n,i+di)
              xsup=SSPAge(isup)
              DO WHILE (agi.GT.xsup)
                 niter=niter+1
                 di=2*di
                 isup=MIN(n,i+di)
                 xsup=SSPAge(isup)
              END DO
           ELSE
              isup=i
              iinf=MAX(1,i-di)
              xinf=SSPAge(iinf)
              DO WHILE(agi.LT.xinf)
                 niter=niter+1
                 di=2*di
                 iinf=MAX(1,i-di)
                 xinf=SSPAge(iinf)
              END DO
           END IF
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (agi<=SSPAge(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  END IF
  i=iinf
  RETURN
END SUBROUTINE Bracket_age
