SUBROUTINE Bracket_metal(zi,i)  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Modules:  
  USE nrtype
  USE Globals
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Local variables 
  IMPLICIT NONE
  INTEGER n,i,di,niter
  INTEGER iinf,isup,imed
  REAL(DP) :: xinf,xsup,Zi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Main text:

  n=nZeta 

  ! begins
  IF (i.EQ.0) THEN
     niter=0
     iinf=1
     isup=n
     IF (zi.GT.SSPZeta(iinf)) THEN
        IF (zi.GE.SSPZeta(isup)) THEN
           iinf=isup-1
        ELSE
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (zi<=SSPZeta(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  ELSE
     niter=0
     iinf=1
     isup=n
     IF (zi.GT.SSPZeta(iinf)) THEN
        IF (zi.GE.SSPZeta(isup)) THEN
           iinf=isup-1
        ELSE
           di=1
           IF (zi.GE.SSPZeta(i)) THEN
              iinf=i
              isup=MIN(n,i+di)
              xsup=SSPZeta(isup)
              DO WHILE (zi.GT.xsup)
                 niter=niter+1
                 di=2*di
                 isup=MIN(n,i+di)
                 xsup=SSPZeta(isup)
              END DO
           ELSE
              isup=i
              iinf=MAX(1,i-di)
              xinf=SSPZeta(iinf)
              DO WHILE(zi.LT.xinf)
                 niter=niter+1
                 di=2*di
                 iinf=MAX(1,i-di)
                 xinf=SSPZeta(iinf)
              END DO
           END IF
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (zi<=SSPZeta(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  END IF
  i=iinf
  RETURN
END SUBROUTINE Bracket_metal