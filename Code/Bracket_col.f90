SUBROUTINE Bracket_COL(n,x,coli,i)  
  !      N :: number of grid vector elements
  !      SSPAge :: gird colour vector
  !      Coli :: Teff variable in grid
  !      Ia :: index of the gird point lower than the Ti value
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Modules
  USE nrtype

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Local variables
  IMPLICIT NONE
  INTEGER n,i,di,niter
  INTEGER iinf,isup,imed
  REAL(DP) x(500),xinf,xsup,coli

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Main text

  IF (i.EQ.0) THEN
     niter=0
     iinf=1
     isup=n
     IF (coli.GT.x(iinf)) THEN
        IF (coli.GE.x(isup)) THEN
           iinf=isup-1
        ELSE
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (coli<=x(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  ELSE
     niter=0
     iinf=1
     isup=n
     IF (coli.GT.x(iinf)) THEN
        IF (coli.GE.x(isup)) THEN
           iinf=isup-1
        ELSE
           di=1
           IF (coli.GE.x(i)) THEN
              iinf=i
              isup=MIN(n,i+di)
              xsup=x(isup)
              DO WHILE (coli.GT.xsup)
                 niter=niter+1
                 di=2*di
                 isup=MIN(n,i+di)
                 xsup=x(isup)
              END DO
           ELSE
              isup=i
              iinf=MAX(1,i-di)
              xinf=x(iinf)
              DO WHILE(coli.LT.xinf)
                 niter=niter+1
                 di=2*di
                 iinf=MAX(1,i-di)
                 xinf=x(iinf)
              END DO
           END IF
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (coli<=x(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  END IF
  i=iinf
  RETURN
END SUBROUTINE Bracket_COL
