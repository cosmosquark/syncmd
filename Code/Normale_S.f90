SUBROUTINE NORMA
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Modules: 
  USE nrtype  
  USE Globals
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
!Local variables
  IMPLICIT NONE
  REAL(DP) Mu, M015Msun, Ml, x, C_norm
! Integral computing for Salpeter normalization. 
  !      / Mu
  !    A |    m^(-x) dm  == N    number     x = 2.35 Salpeter
  !      / Ml
  !
  !      / Mu
  !    A | m^(-x+1) dm   == M    mass
  !      / Ml

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! !Main text
  Mu=100                       !max massa to check
  M015Msun=0.15                !min massa in SSP
  Ml=0.08                      !min mass in solar mass
  x = 2.25                     !Salpeter 
  NTOTSTAR_OverMl = NTOTSTAR   !num tot stars in SSP in YZVAR

  WRITE(*,201) Ml
  201 FORMAT(2x, 'Lower mass in solar mass= ', 1f10.3)

  !norm constant 
  C_norm = NTOTSTAR * (Ml*Mu)**x *(x-1) / (Mu**x * Ml - Mu * Ml ** x)

  ! num stars under Ml=0.15 M0
  NStar_UnderMl=  C_norm/(1.-x) * ( M015Msun**(1.-x) - Ml**(1.-x) )
  WRITE(*,200) NTOTSTAR_OverMl,  NStar_UnderMl
  200 FORMAT(2x, 'Number of stars OVER/UNDER the lower mass = ', i10,3x, i10)
  
  ! mean mass in upper range
  Mean_Mass_OverMl = ( (1./(-x+2.)) * ( Mu**(-x+2.) - M015Msun**(-x+2.)) ) / ( (1./(-x+1.)) * ( Mu**(-x+1.) - M015Msun**(-x+1.)) )
  Mean_Mass_UnderMl = ( (1./(-x+2.)) * ( M015Msun**(-x+2.) - Ml**(-x+2.)) )/ ( (1./(-x+1.)) * ( M015Msun**(-x+1.) - Ml**(-x+1.)) )
  WRITE(*,300)  Mean_Mass_OverMl, Mean_Mass_UnderMl
  300 FORMAT(5x, 'Mean mass OVER/UNDER the lower mass =', 1f10.3,3x, 1f10.3)
  
  !mean mass in SSP
  Mean_Mass_Stars = (NTOTSTAR_OverMl* Mean_Mass_OverMl + NStar_UnderMl * Mean_Mass_UnderMl)/(NTOTSTAR_OverMl+NStar_UnderMl)
  WRITE(*,400) Mean_Mass_Stars
  400 FORMAT(5x, 'Mean mass in the SSP =', 1p1e12.3)
  
  !mass SSP
  Total_Mass_SSP = (NTOTSTAR_OverMl* Mean_Mass_OverMl + NStar_UnderMl * Mean_Mass_UnderMl)
  WRITE(*,500)  Total_Mass_SSP
  500 FORMAT(5x, 'Total Mass in the SSP = ', 1p1e12.3)
END SUBROUTINE NORMA
