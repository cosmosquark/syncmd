SUBROUTINE Bracket_MAG(n,x,magi,i)  
  !     N :: number of grid vector elements
  !     x :: gird colour vector
  !     ...the same as the other routines...
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Modules: 
  USE nrtype

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Local variables:
  IMPLICIT NONE
  INTEGER n,i,di,niter
  INTEGER iinf,isup,imed
  REAL(DP) x(500),xinf,xsup,magi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Main text:
  
  IF (i.EQ.0) THEN
     niter=0
     iinf=1
     isup=n
     IF (magi.GT.x(iinf)) THEN
        IF (magi.GE.x(isup)) THEN
           iinf=isup-1
        ELSE
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (magi<=x(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  ELSE
     niter=0
     iinf=1
     isup=n
     IF (magi.GT.x(iinf)) THEN
        IF (magi.GE.x(isup)) THEN
           iinf=isup-1
        ELSE
           di=1
           IF (magi.GE.x(i)) THEN
              iinf=i
              isup=MIN(n,i+di)
              xsup=x(isup)
              DO WHILE (magi.GT.xsup)
                 niter=niter+1
                 di=2*di
                 isup=MIN(n,i+di)
                 xsup=x(isup)
              END DO
           ELSE
              isup=i
              iinf=MAX(1,i-di)
              xinf=x(iinf)
              DO WHILE(magi.LT.xinf)
                 niter=niter+1
                 di=2*di
                 iinf=MAX(1,i-di)
                 xinf=x(iinf)
              END DO
           END IF
           DO WHILE (iinf+1.LT.isup)
              niter=niter+1
              imed=(iinf+isup)/2
              IF (magi<=x(imed)) THEN
                 isup=imed
              ELSE
                 iinf=imed
              END IF
           END DO
        END IF
     END IF
  END IF
  i=iinf
  RETURN
END SUBROUTINE Bracket_MAG


